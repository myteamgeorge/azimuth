import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.crawler.sistemasinternos.helper.ArquivoHelper;
import com.crawler.sistemasinternos.model.Acesso;

class ArquivoHelperTeste {

	private static final String LOGIN = "login";
	private static final String SENHA = "senha";
	private static final int ROBOS = 2;

	@BeforeAll
	public static void preparaArquivoAcesso() throws Exception {
		if(ArquivoHelper.carregarArquivo() == null){
			System.out.println("Novo arquivo de dados criado.");
			Acesso acesso = new Acesso();
			acesso.setLogin(LOGIN);
			acesso.setSenha(SENHA);
			acesso.setRobos(ROBOS);
			ArquivoHelper.salvarAcesso(acesso);
		} else{
			System.out.println("Já existe arquivo de dados criado.");
		}
	}

	@Test
	public void lerLoginSenhaArquivo() throws Exception {
		String loginSenha = ArquivoHelper.carregarLoginSenhaPrimeiraLinha();
		
		assert(loginSenha.indexOf(";") > -1);
		
		String[] dados = loginSenha.split(";");

		assert(!dados[0].isEmpty());
		assert(!dados[1].isEmpty());
		
	}
	
	@Test
	public void lerQuantidadeRobosArquivo() throws Exception {
		int quantidade = ArquivoHelper.carregarQuantidadeRobos();
		assert(quantidade > 0);
	}
	
}

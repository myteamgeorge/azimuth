package com.crawler.sistemasinternos.services;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.FirebaseDatabase;

@Component
public class FirebaseService {
	
	
	String MENSAGEM = "Processando registro {0} de {1}...";

	private FirebaseDatabase defaultDatabase;

	public FirebaseService() {

		FileInputStream serviceAccount;
		try {
			serviceAccount = new FileInputStream("firebase-credentials.json");

			FirebaseOptions options;
			options = new FirebaseOptions.Builder().setCredential(FirebaseCredentials.fromCertificate(serviceAccount))
					.setDatabaseUrl("https://azimuth-9000b.firebaseio.com").build();

			FirebaseApp.initializeApp(options);

			this.defaultDatabase = FirebaseDatabase.getInstance();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void atualizarWebVendas(int contagem, int total) {
		try {
			defaultDatabase.getReference("webVendas/mensagem").setValue(MessageFormat.format(MENSAGEM, contagem, total));
		}catch(Exception e){
			System.out.println("Falha ao atualizar contagem");
			e.printStackTrace();
		}
	}

	public void finalizarWebVendas() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		String data = sdf.format(new Date());
		defaultDatabase.getReference("webVendas/mensagem").setValue("Nenhum processamento rodando no momento! O último processamento foi finalizado em " + data);
		
	}
	
	public void salvarArquivo(){
		
	}

	public static void main(String[] args) {
		/*FirebaseService service = new FirebaseService();

		for (int a = 0; a <= 1000; a++)
			service.atualizarWebVendas(a, 1000);*/
	}
}

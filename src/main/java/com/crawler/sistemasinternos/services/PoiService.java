package com.crawler.sistemasinternos.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.crawler.sistemasinternos.dto.WebVendasDadosResponse;

@Component
public class PoiService {

	private final String ARQUIVO_DOCS = "arquivos/planilha-entrada.xlsx";
	private final String ARQUIVO_DOCS_PROCESSADOS = "arquivos/planilha-saida.xlsx";

	public void salvarArquivo(MultipartFile file) throws IOException {

		File convFile = new File(ARQUIVO_DOCS);
		convFile.createNewFile();
		FileOutputStream fos;

		fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
	}

	public List<String> lerDocumentos() throws IOException {
		List<String> retorno = new ArrayList<>();
		String CPF = "CPF";
		String CNPJ = "CNPJ";

		FileInputStream file = new FileInputStream(new File(ARQUIVO_DOCS));

		if (file != null) {
			XSSFWorkbook workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);

			DataFormatter formatter = new DataFormatter();

			int linhas = sheet.getPhysicalNumberOfRows();
			for (int a = 0; a < linhas; a++) {

				XSSFRow linha = sheet.getRow(a);
				Cell cell = linha.getCell(0);

				String valor = formatter.formatCellValue(cell);

				if (valor != null && !valor.replaceAll("0", "").replaceAll("1", "").isEmpty() && valor.replaceAll("[0-9/.-]", "").isEmpty()) {
					valor = valor.replaceAll("\"", "").replaceAll(" ", "").replaceAll("/", "").replaceAll("-", "")
							.replaceAll("\\.", "");

					String tipo = valor.length() <= 11 ? CPF : CNPJ;

					while ((tipo.equals(CPF) && valor.length() < 11) || (tipo.equals(CNPJ) && valor.length() < 14)) {
						valor = "0" + valor;
					}

					if (!valor.isEmpty()) {

						if (!retorno.contains(valor)) {
							retorno.add(valor);
						}
					}

				}else {
					System.out.println("[ERRO] Ignorando processamento de documento " + valor);
				}

			}

			workbook.close();
		}

		return retorno;
	}

	public File gerarArquivoDados(List<WebVendasDadosResponse> dados) {
		File retorno = null;
		String SHEET = "Liberados";
		String SHEET_ATIVO = "Ativos";
		XSSFWorkbook workbook = new XSSFWorkbook();

		XSSFSheet sheet = workbook.createSheet(SHEET);
		XSSFSheet sheetAtivos = workbook.createSheet(SHEET_ATIVO);

		//gerarTxt(dados);

		for (WebVendasDadosResponse dado : dados) {

			XSSFRow linha = null;

			boolean isAtivo = dado.getDados() != null && dado.getDados().isAtivo()
					&& (dado.getError() == null || dado.getError().isEmpty());

			if (!isAtivo) {
				int num = sheet.getLastRowNum() + 1;
				linha = sheet.createRow(num);
				System.out.println("Gerando XLSX [livres] [linha " + num + "]");
			} else {

				int num = sheetAtivos.getLastRowNum() + 1;
				linha = sheetAtivos.createRow(num);
				System.out.println("Gerando XLSX [ativo] [linha " + num + "]");
			}

			if (dado.getDados() != null) {
				for (Object valor : dado.getDados().asArray()) {

					gerar(valor, linha);

				}
			}

		}

		try {
			FileOutputStream out = new FileOutputStream(ARQUIVO_DOCS_PROCESSADOS);

			workbook.write(out);
			workbook.close();
			out.close();

			System.out.println("Arquivo final gerado com " + dados.size() + " linhas no total [" + new Date() + "]");

			retorno = getArquivoFinal();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return retorno;
	}

	private void gerarTxt(List<WebVendasDadosResponse> dados) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy HH mm ss");
		String data = sdf.format(new Date());
		File file = new File("arquivos/csv/"+data+".csv");

		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(file);

			for (WebVendasDadosResponse dado : dados) {

				StringBuilder dadosStr = new StringBuilder();

				Object[] dadosArray = dado.getDados().asArray();

				for(Object obj: dadosArray){
					String objString = (String) obj;
					if(!dadosStr.toString().isEmpty()){
						dadosStr.append(";");
					}
					dadosStr.append(objString != null ? objString : "");
				}
						
				dadosStr.append("\n");
				fileWriter.write(dadosStr.toString());

			}
			

			fileWriter.flush();
			fileWriter.close();

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	private void gerar(Object valor, XSSFRow linha) {
		String valorStr = (valor != null ? valor.toString() : "");

		XSSFCell cell = linha.createCell(linha.getLastCellNum() + 1);
		cell.setCellValue(valorStr);
	}

	public File getArquivoFinal() {
		return new File(ARQUIVO_DOCS_PROCESSADOS);
	}

}

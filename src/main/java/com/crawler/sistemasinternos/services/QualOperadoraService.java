package com.crawler.sistemasinternos.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crawler.sistemasinternos.crawlers.QualOperadoraCrawler;
import com.crawler.sistemasinternos.dto.QualOperadoraResponse;

@Component
public class QualOperadoraService {

	@Autowired
	private QualOperadoraCrawler crawler;
	
	public List<QualOperadoraResponse> busca(List<String> numeros){
		List<QualOperadoraResponse> retorno = new ArrayList<>();
		
		for(String numero: numeros){

			try {
				
				QualOperadoraResponse dado = this.buscaSimples(numero);
				retorno.add(dado);

				Thread.sleep(6000);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
		
		return retorno;		
	}

	public QualOperadoraResponse buscaSimples(String numero) {
		QualOperadoraResponse dado = new QualOperadoraResponse();
		
		try {
			
			dado = crawler.pesquisar(numero);
		} catch (IOException | InterruptedException e) {
			
			e.printStackTrace();
			dado.setError(e.getMessage());
		}
		
		return dado;
	}

	
}

package com.crawler.sistemasinternos.services;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.AuthenticationException;

import com.crawler.sistemasinternos.helper.ArquivoHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crawler.sistemasinternos.dto.WebVendasDadosRequest;
import com.crawler.sistemasinternos.dto.WebVendasDadosResponse;
import com.crawler.sistemasinternos.threads.WebVendasDadosThread;

@Component
public class WebVendasDadosService {

	@Autowired
	private PoiService poiService;

	@Autowired
	private FirebaseService firebaseService;

	public List<WebVendasDadosResponse> busca(String[] documentos) {

		int quantidadeThreads = 0;
		try {
			quantidadeThreads = ArquivoHelper.carregarQuantidadeRobos();
		} catch (Exception e) {
			e.printStackTrace();
			quantidadeThreads = 10;
		}

		if(documentos.length < quantidadeThreads){
			quantidadeThreads = documentos.length;
		}
		
		List<WebVendasDadosThread> threads = new ArrayList<>();
		List<List<WebVendasDadosRequest>> dados = new ArrayList<>();
		int quantidadePorList = documentos.length / quantidadeThreads;
		int cursor = 0;
		
		for(int a=0; a<quantidadeThreads; a++){
			 

			List<WebVendasDadosRequest> list = new ArrayList<>();
		
			if(a == quantidadeThreads){
				quantidadePorList = documentos.length - cursor;
			}
			
			int limite = cursor+quantidadePorList;
			while( cursor < limite ){

				WebVendasDadosRequest dadosRequest = new WebVendasDadosRequest();
				dadosRequest.setDocumento(documentos[cursor]);
				list.add(dadosRequest);
				
				cursor++;
			}
			
			dados.add(list);
		}
		
		
		int idThread = 0;
		for(List<WebVendasDadosRequest> dado: dados){
			String nomeThread = "THREAD-" + idThread++;
			WebVendasDadosThread thread = new WebVendasDadosThread(nomeThread, dado);
			
			Thread t1 = new Thread(thread);
			t1.start();
			
			threads.add(thread);
		}
		

		while ( isThreadActive(threads) ) {
			try {

				logarThreads(threads);
				
				int processados = getContagemProcessados(threads);

				firebaseService.atualizarWebVendas(processados, documentos.length);

				Thread.sleep(10000);
			} catch (InterruptedException e) {
				System.out.println("[ERRO ATUALIZAR FIREBASE] " + e.getMessage());
			}
		}

		List<WebVendasDadosResponse> listaFinal = getListaFinalResultados(threads);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
		String data = sdf.format(new Date());
		
		System.out.println("Nao existe processamento rodando no momento. O ultimo terminou em "+data+" com " + listaFinal.size() + " resultados.");
		firebaseService.finalizarWebVendas();

		return listaFinal;
	}

	private List<WebVendasDadosResponse> getListaFinalResultados(List<WebVendasDadosThread> threads) {
		List<WebVendasDadosResponse> resultadoFinal = new ArrayList<>();

		for(WebVendasDadosThread thread : threads){
			resultadoFinal.addAll(thread.getResposta());
		}
		
		return resultadoFinal;
	}

	private int getContagemProcessados(List<WebVendasDadosThread> threads) {
		
		int total = 0;
		
		for(WebVendasDadosThread thread : threads){
			total = total + thread.getResposta().size();
		}
		
		return total;
	}

	private void logarThreads(List<WebVendasDadosThread> threads) {
		
		StringBuilder log = new StringBuilder();
		
		for(WebVendasDadosThread thread: threads){
			log.append("["+thread.getNomeThread()+":"+thread.isAtivo());
		}
		
		System.out.println(log.toString());
		
	}

	private boolean isThreadActive(List<WebVendasDadosThread> threads) {
		for(WebVendasDadosThread thread: threads){

			if(thread.isAtivo()){
				return Boolean.TRUE;
			}
			
		}

		return Boolean.FALSE;
	}

	public File carregarDados() throws AuthenticationException, IOException {

		List<String> documentos = poiService.lerDocumentos();
		String[] array = {};
		array = documentos.toArray(array);

		List<WebVendasDadosResponse> dados = this.busca(array);

		File retorno = poiService.gerarArquivoDados(dados);

		return retorno;
	}

}

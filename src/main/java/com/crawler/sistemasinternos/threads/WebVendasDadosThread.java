package com.crawler.sistemasinternos.threads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.crawler.sistemasinternos.crawlers.WebVendasDadosCrawler;
import com.crawler.sistemasinternos.dto.WebVendasDadosRequest;
import com.crawler.sistemasinternos.dto.WebVendasDadosResponse;
import com.crawler.sistemasinternos.model.WebVendasDados;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;

public class WebVendasDadosThread implements Runnable{

	public WebVendasDadosThread(String nomeThread, List<WebVendasDadosRequest> dados){
		
		try {
			this.nomeThread = nomeThread;
			this.dados = dados;
			this.resposta = new ArrayList<>();
			this.ativo = Boolean.TRUE;
			this.crawler = new WebVendasDadosCrawler();
			this.ultimaAcao = new Date();
		} catch (FailingHttpStatusCodeException | IOException e) {
			try {
				this.crawler = new WebVendasDadosCrawler();
			} catch (IOException | FailingHttpStatusCodeException e1) {
				System.out.println("Erro na instancia da THREAD: " + this.nomeThread + " | " + e.getMessage());
			}
			System.out.println("Erro na instancia da THREAD: " + this.nomeThread + " | " + e.getMessage());
		}
		
	}
	
	private String nomeThread;
	private List<WebVendasDadosRequest> dados;
	private WebVendasDadosCrawler crawler;
	private List<WebVendasDadosResponse> resposta;
	private Date ultimaAcao;
	private Boolean ativo;
	
	@Override
	public void run() {

		int contagem = 1;
		for(WebVendasDadosRequest request : dados){
			try {
				
				WebVendasDadosResponse busca = crawler.pesquisar(request.getDocumento(), contagem++, dados.size(), this.nomeThread);
				this.resposta.add(busca);

			}catch(Exception e){

				WebVendasDadosResponse buscaVazia = new WebVendasDadosResponse();
				WebVendasDados webDadosVendasVazio = new WebVendasDados();
				
				buscaVazia.setDocumento(request.getDocumento());
				webDadosVendasVazio.setDocumento(request.getDocumento());
				buscaVazia.setDados(webDadosVendasVazio);
				this.resposta.add(buscaVazia);
				
				System.out.println("[ERRO] ["+this.nomeThread+"] ["+request.getDocumento()+"] ["+e.getMessage()+"]");
			}
			
			this.ultimaAcao = new Date();
			
		}
		
		this.ativo = Boolean.FALSE;
	}
	
	public Boolean isAtivo(){
		if(this.isOld()){
			
			try {
				this.ultimaAcao = Calendar.getInstance().getTime();
				this.crawler = new WebVendasDadosCrawler();
				
				this.controlaProcessados();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return this.ativo;
	}
	
	private void controlaProcessados() {
		
		List<WebVendasDadosRequest> aRemover = new ArrayList<>();
		
		for(WebVendasDadosResponse processado: this.resposta){
			
			for(WebVendasDadosRequest processar: this.dados){
				if(processado.getDados() != null && compareDocumentos(processado.getDados().getDocumento(), processar.getDocumento())){
					aRemover.add(processar);
					break;
				}
			}
			
		}
		
		this.dados.removeAll(aRemover);
	}

	private boolean compareDocumentos(String documento, String documento2) {

		String doc1 = documento.replaceAll("[\\&\\.\\_\\-\\(\\) ]", "");
		String doc2 = documento2.replaceAll("[\\&\\.\\_\\-\\(\\) ]", "");
		
		return doc1.equals(doc2);
	}

	private boolean isOld() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.ultimaAcao);
		cal.add(Calendar.MINUTE, 2);
		
		Date dataAtual = Calendar.getInstance().getTime();
		
		return dataAtual.after(cal.getTime());
	}
	
	public List<WebVendasDadosResponse> getResposta(){
		return this.resposta;
	}

	public Date getUltimaAcao() {
		return ultimaAcao;
	}

	public String getNomeThread() {
		return nomeThread;
	}

}

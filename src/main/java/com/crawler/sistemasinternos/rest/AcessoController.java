package com.crawler.sistemasinternos.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.crawler.sistemasinternos.helper.ArquivoHelper;
import com.crawler.sistemasinternos.model.Acesso;

@Controller
public class AcessoController {

	@RequestMapping(method = RequestMethod.POST, path = "/novo-acesso")
	@ResponseBody
	public ResponseEntity salvarDadosAcesso(@RequestBody Acesso acesso) {

		ResponseEntity resp;

		try {
			acesso.setRobos(ArquivoHelper.carregarQuantidadeRobos());
			ArquivoHelper.salvarAcesso(acesso);

			resp = ResponseEntity.ok().build();
		} catch (Exception e) {
			resp = ResponseEntity.badRequest().build();
			e.printStackTrace();
		}

		return resp;

	}

	@RequestMapping(method = RequestMethod.POST, path = "/quantidade-robos")
	@ResponseBody
	public ResponseEntity alterarQuantidadeRobos(@RequestBody Acesso acesso) {

		ResponseEntity resp;

		try {

			acesso.setLoginSenhaFromString(ArquivoHelper.carregarLoginSenhaPrimeiraLinha());
			acesso.setRobos(acesso.getRobos());

			ArquivoHelper.salvarAcesso(acesso);

			resp = ResponseEntity.ok().build();
		} catch (Exception e) {
			resp = ResponseEntity.badRequest().build();
			e.printStackTrace();
		}

		return resp;

	}

	@RequestMapping(method = RequestMethod.GET, path = "/acesso")
	@ResponseBody
	public ResponseEntity get() {

		ResponseEntity resp;

		try {
			Acesso acesso = new Acesso();
			acesso.setLoginSenhaFromString(ArquivoHelper.carregarLoginSenhaPrimeiraLinha());
			acesso.setRobos(ArquivoHelper.carregarQuantidadeRobos());

			resp = ResponseEntity.ok().body(acesso);
		} catch (Exception e) {
			resp = ResponseEntity.badRequest().build();
			e.printStackTrace();
		}

		return resp;

	}

}

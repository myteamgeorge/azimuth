package com.crawler.sistemasinternos.rest;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.crawler.sistemasinternos.services.PoiService;

@Controller
public class UploadController {

	@Autowired
	private PoiService poiService;

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity UploadFile(@RequestParam(value = "file", required = true) MultipartFile file) {

		ResponseEntity retorno;

		try {

			poiService.salvarArquivo(file);
			retorno = ResponseEntity.ok().build();
		} catch (IOException e) {

			e.printStackTrace();
			retorno = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}

		return retorno;
	}

}
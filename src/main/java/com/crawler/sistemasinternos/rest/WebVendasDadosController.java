package com.crawler.sistemasinternos.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.crawler.sistemasinternos.services.PoiService;
import com.crawler.sistemasinternos.services.WebVendasDadosService;

@Controller
public class WebVendasDadosController {

	@Autowired
	private WebVendasDadosService service;

	@Autowired
	private PoiService poiService;

	@RequestMapping(method = RequestMethod.GET, path = {"/processar*", "/webVendas/processar*"})
	public void list(final HttpServletRequest request, final HttpServletResponse response) {

		try {

			File file = service.carregarDados();

			OutputStream output = response.getOutputStream();

			response.reset();
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

			InputStream input = new FileInputStream(file);

			IOUtils.copyLarge(input, output);
			output.flush();

		} catch (AuthenticationException | IOException e) {

			e.printStackTrace();
		}

	}

	@RequestMapping(method = RequestMethod.GET, path = {"/download*", "/webVendas/download*"})
	public void getFile(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		response.reset();
		File file = poiService.getArquivoFinal();
		if (file.exists()) {

			OutputStream output = response.getOutputStream();

			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			// response.setContentLength((int) (file.length()));
			response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");

			InputStream input = new FileInputStream(file);

			IOUtils.copyLarge(input, output);
			output.flush();

		} else {
			response.sendError(1);
		}

	}

}

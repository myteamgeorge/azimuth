package com.crawler.sistemasinternos.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.crawler.sistemasinternos.dto.QualOperadoraResponse;
import com.crawler.sistemasinternos.services.QualOperadoraService;

@Controller
public class QualOperadoraController {

	@Autowired
	private QualOperadoraService service;

	@RequestMapping(method = RequestMethod.GET, path = "/qualOperadora/get/{numero}")
	@ResponseBody
	public ResponseEntity get(@PathVariable(name = "numero") String numero) {

		ResponseEntity<QualOperadoraResponse> response;

		QualOperadoraResponse dados = service.buscaSimples(numero);
		response = ResponseEntity.ok(dados);

		return response;
	}

	@RequestMapping(method = RequestMethod.POST, path = "/qualOperadora/list/")
	@ResponseBody
	public ResponseEntity list(@RequestBody List<String> numeros) {

		ResponseEntity<List<QualOperadoraResponse>> response;

		List<QualOperadoraResponse> dados = service.busca(numeros);
		response = ResponseEntity.ok(dados);

		return response;
	}

}

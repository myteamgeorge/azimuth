package com.crawler.sistemasinternos.crawlers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.AuthenticationException;

import org.w3c.dom.html.HTMLLIElement;

import com.crawler.sistemasinternos.dto.WebVendasDadosResponse;
import com.crawler.sistemasinternos.enumerator.URL;
import com.crawler.sistemasinternos.helper.ArquivoHelper;
import com.crawler.sistemasinternos.model.WebVendasDados;
import com.crawler.sistemasinternos.model.WebVendasDadosContato;
import com.crawler.sistemasinternos.model.WebVendasDadosInstalacao;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTableCell;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

public class WebVendasDadosCrawler extends BaseCrawler {

    private String postLoginURL;

    public WebVendasDadosCrawler() throws FailingHttpStatusCodeException, IOException {
        super(URL.WEBVENDAS.getPath()+"/siebelview/dealer/", true);
    }

    public void login() throws Exception {

        Thread.sleep(2000);

        HtmlTextInput login = (HtmlTextInput) this.page.getElementById("login");
        HtmlPasswordInput senha = (HtmlPasswordInput) this.page.getElementById("senha");

        String acesso = ArquivoHelper.carregarLoginSenhaPrimeiraLinha();
        login.setText(acesso.split(";")[0]);
        senha.setText(acesso.split(";")[1]);

        HtmlSubmitInput submit = (HtmlSubmitInput) this.page.getFirstByXPath("//*[@id='formLogin']/center/input");
        this.page = submit.click();

        int count = 0;
        boolean achou = false;

        while (count < 10 && !achou) {
            this.getWebClient().waitForBackgroundJavaScript(1000L);
            if (this.page.getElementById("login") != null) {

                if (this.page.asText().contains("Erro ao conectar")) {

                    StringBuilder sb = new StringBuilder();

                    for (DomElement elem : this.page.getElementsByTagName("li")) {
                        HTMLLIElement li = (HTMLLIElement) elem;
                        sb.append(li.getTextContent());
                    }

                    System.out.println("Nao achou campo de login. Continuando com processo..." + sb.toString());
                    throw new AuthenticationException("Falha ao realizar login: " + sb.toString());
                }
                count++;
                System.out.println("Achou campo de login.. aguardando mais um tempo...");
            } else {
                break;
            }
        }

    }

    @SuppressWarnings("unused")
    public WebVendasDadosResponse pesquisar(String nrDoc, int contagem, int total, String nomeThread)
            throws Exception {

        String ELEMENT_DOC_NUMBER = "//*[@id='formTemplate:documentNumber']";
        String ELEMENT_PESQUISAR = "//*[@id='formTemplate:pesquisar']";

        if(!verificacao()) {
            login();
        }

        controlaPaginaOriginalPostLogin();

        WebVendasDadosResponse retorno = new WebVendasDadosResponse();

        HtmlTextInput documento = (HtmlTextInput) this.page.getFirstByXPath(ELEMENT_DOC_NUMBER);

        if (documento != null) {
            documento.setText(nrDoc);

            HtmlButtonInput pesquisarButton = (HtmlButtonInput) this.page.getFirstByXPath(ELEMENT_PESQUISAR);
            HtmlPage resultado = (HtmlPage) pesquisarButton.click();

            WebVendasDados dadosWebVendas = preencheRetorno(nrDoc, contagem, total, nomeThread);
            retorno.setDados(dadosWebVendas);
            retorno.setStatus(200);

        } else {
            WebVendasDados webVendasDados = new WebVendasDados();
            webVendasDados.setDocumento(nrDoc);

            retorno.setError("Não encontrou dados para o documento " + nrDoc);
            retorno.setDados(webVendasDados);
            retorno.setDocumento(nrDoc);
            retorno.setStatus(404);
        }

        if (retorno != null && retorno.getDados() != null
                && (retorno.getDados().getDocumento() == null || retorno.getDados().getDocumento().isEmpty())) {
            retorno.getDados().setDocumento(nrDoc);
            retorno.setDocumento(nrDoc);
        }

        return retorno;
    }

    private void controlaPaginaOriginalPostLogin()
            throws FailingHttpStatusCodeException, MalformedURLException, IOException {
        if (this.postLoginURL == null) {
            this.postLoginURL = this.page.getPage().getBaseURL().getPath();
        } else {
            this.page = this.getWebClient().getPage(URL.WEBVENDAS.getPath() + this.postLoginURL);
        }
    }

    private boolean waitFor(String xpath, Boolean shouldBeThere, String doc, int contagem, int total, String nomeThread) {
        int count = 1;
        boolean achou = false;
        boolean vazio = false;

        if(this.page == null){
            return false;
        }

        while (count++ < 22 && !achou) {

            if(!xpath.equals("//*[@id=\"popups:formPopupAddressInfo:doneDateOrder\"]")){
                try{
                    HtmlTextInput nome = (HtmlTextInput) this.page.getFirstByXPath("//*[@name='formTemplate:j_id421']");
                    HtmlTextInput preencheEmail = (HtmlTextInput) this.page
                            .getFirstByXPath("//*[@id='formTemplate:emailDomaincomboboxField']");

                    if (nome != null || preencheEmail != null) {
                        vazio = true;
                        break;
                    }
                }catch(Exception e){
                    System.out.println("["+nomeThread+"] " + e.getMessage());
                }
            }

            this.getWebClient().waitForBackgroundJavaScript(500L);
            if ((this.page.getFirstByXPath(xpath) != null && !shouldBeThere)
                    || (shouldBeThere && this.page.getFirstByXPath(xpath) == null)) {
                this.page = (HtmlPage) this.getWebClient().getCurrentWindow().getEnclosedPage();

                continue;
            } else {
                achou = true;
            }

            break;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        System.out.println("["+nomeThread+"] [" + sdf.format(new Date()) + "] [" + contagem + "/" + total + "] [" + doc + "] [" + count
                + " vezes] [Achou: " + achou + "] [Vazio: " + vazio + "]");
        return achou;

    }

    private boolean verificacao() throws AuthenticationException {
        if (this.page.asText().contains("Access Denied") || this.page.asText().contains("Entrar")) {
            return false;
        }
        
        return true;
    }

    private WebVendasDados preencheRetorno(String nrDoc, int contagem, int total, String nomeThread) {

        WebVendasDados webVendasDados = new WebVendasDados();

        String xpathTabela = "//*[@id='formTemplate:dtDealerServiceAddress:tb']/tr";

        Boolean achouTelaRetorno = waitFor(xpathTabela, true, nrDoc, contagem, total, nomeThread);

        if (achouTelaRetorno) {

            HtmlTextInput nome = (HtmlTextInput) this.page.getFirstByXPath("//*[@name='formTemplate:j_id422']");
            HtmlTextInput doc = (HtmlTextInput) this.page.getFirstByXPath("//*[@id='formTemplate:documento']");
            HtmlTextInput segmento = (HtmlTextInput) this.page.getFirstByXPath("//*[@name='formTemplate:j_id426']");
            HtmlSelect contato = (HtmlSelect) this.page.getFirstByXPath("//*[@name='formTemplate:cbContato']");

            HtmlTextInput hrContatoInicial = (HtmlTextInput) this.page
                    .getFirstByXPath("//*[@name='formTemplate:j_id452']");
            HtmlTextInput hrContatoFinal = (HtmlTextInput) this.page
                    .getFirstByXPath("//*[@name='formTemplate:j_id454']");

            if (doc != null) {
                webVendasDados.setDocumento(doc.asText());
            }
            if (nome != null) {
                webVendasDados.setNome(nome.asText());
            }
            if (segmento != null) {
                webVendasDados.setSegmento(segmento.asText());
            }
            if (hrContatoInicial != null) {
                webVendasDados.setHoraContatoInicial(hrContatoInicial.asText());
            }
            if (hrContatoFinal != null) {
                webVendasDados.setHoraContatoFinal(hrContatoFinal.asText());
            }

            // lista para controle de contatos duplicados
            List<String> contatosProcessados = new ArrayList<>();

            if (contato != null) {
                for (HtmlOption option : contato.getOptions()) {
                    String valor = option.asText();
                    if (!valor.replace(" ", "").equals("Selecione")) {

                        String valorTratado = valor.replaceAll("[\\&\\.\\_\\-\\(\\) ]", "");
                        if (!contatosProcessados.contains(valorTratado)) {
                            // popula lista de controle de contatos duplicados
                            contatosProcessados.add(valorTratado);

                            WebVendasDadosContato webVendasDadosContato = new WebVendasDadosContato(valor);
                            webVendasDados.getWebVendasDadosContato().add(webVendasDadosContato);
                        }

                    }
                }
            }

            List<HtmlTableRow> linhaList = this.page.getByXPath(xpathTabela);

            for (HtmlTableRow linha : linhaList) {
                List<String> colunas = new ArrayList<>();

                for (HtmlTableCell column : linha.getCells()) {
                    String value = column.asText();
                    colunas.add(value);
                }

                WebVendasDadosInstalacao webVendasDadosInstalacao = new WebVendasDadosInstalacao(colunas);

                if(webVendasDadosInstalacao.isAtivo()) {
                    try {
                        String dataConlusao = getDataConclusao(nrDoc, linhaList.indexOf(linha), contagem, total, nomeThread);
                        webVendasDadosInstalacao.setDataConclusao(dataConlusao);
                    } catch (IOException e) {
                        System.out.println("FALHA AO PREENCHER DATA DE CONCLUSÃO...");
                        e.printStackTrace();
                    }
                }

                webVendasDados.getWebVendasDadosInstalacao().add(webVendasDadosInstalacao);
            }

        }

        return webVendasDados;
    }

    public String getDataConclusao(String nrDoc, int indexItemLista, int contagem, int total, String nomeThread) throws IOException {

        String data = "";
        String dataConclusaoXPATH = "//*[@name=\"popups:formPopupAddressInfo:doneDateOrder\"]";


        String botaoDetalhesInstalacaoXPATH = "//*[@id=\"formTemplate:dtDealerServiceAddress:"+indexItemLista+":j_id500:0:btnAddressInfo\"]";
        HtmlAnchor detalhes =  (HtmlAnchor) this.page.getFirstByXPath(botaoDetalhesInstalacaoXPATH);

        this.page = detalhes.click();

        if(this.waitFor(dataConclusaoXPATH, true, nrDoc, contagem, total, nomeThread)) {

            HtmlTextInput dataConlusao = (HtmlTextInput) this.page.getFirstByXPath(dataConclusaoXPATH);

            if(dataConlusao != null) {

                //procurando por data
                int count = 0;

                while(data.isEmpty() && count++ < 5) {
                    this.getWebClient().waitForBackgroundJavaScript(1000L);
                    dataConlusao = (HtmlTextInput) this.page.getFirstByXPath(dataConclusaoXPATH);
                    if(dataConlusao != null) {
                        data = dataConlusao.asText();
                    }
                }
            }else {
                System.out.println("dataConlusao é NULO");
            }

        }else {
            System.out.println("waitFor failed " + dataConclusaoXPATH);
        }

        return data;
    }

}

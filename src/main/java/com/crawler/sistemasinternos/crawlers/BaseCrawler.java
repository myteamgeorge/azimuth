
package com.crawler.sistemasinternos.crawlers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.ScriptException;
import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HTMLParserListener;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.JavaScriptErrorListener;

public class BaseCrawler {

	private WebClient webClient;
	public HtmlPage page;

	public BaseCrawler() {
	}

	public BaseCrawler(String url) throws FailingHttpStatusCodeException, MalformedURLException, IOException {

		this(url, true);
	}

	public BaseCrawler(String url, Boolean jsEnabled)
			throws FailingHttpStatusCodeException, MalformedURLException, IOException {

		try {
			this.webClient = new WebClient(BrowserVersion.CHROME);

			this.webClient.getOptions().setUseInsecureSSL(Boolean.TRUE);
			this.webClient.getOptions().setJavaScriptEnabled(jsEnabled);
			this.webClient.getOptions().setCssEnabled(false);
			this.webClient.waitForBackgroundJavaScript(10000);
			this.webClient.waitForBackgroundJavaScriptStartingBefore(10000);

			override(this.webClient);

			this.webClient.getOptions().setTimeout(90000);
			this.page = (HtmlPage) this.webClient.getPage(url);
		} catch (Exception e) {

		}

	}

	private void override(WebClient webClient) {
		java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);
		java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);


		this.webClient.setCssErrorHandler(new SilentCssErrorHandler());
		this.webClient.getOptions().setThrowExceptionOnScriptError(false);
		this.webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		this.webClient.getOptions().setPrintContentOnFailingStatusCode(false);
		
		webClient.setJavaScriptErrorListener(new JavaScriptErrorListener() {

			@Override
			public void timeoutError(HtmlPage arg0, long arg1, long arg2) {
			}
			@Override
			public void scriptException(HtmlPage arg0, ScriptException arg1) {
			}
			@Override
			public void malformedScriptURL(HtmlPage arg0, String arg1, MalformedURLException arg2) {
			}
			@Override
			public void loadScriptError(HtmlPage arg0, URL arg1, Exception arg2) {
			}
		});
		webClient.setHTMLParserListener(new HTMLParserListener() {

			@Override
			public void warning(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
			}
			@Override
			public void error(String arg0, URL arg1, String arg2, int arg3, int arg4, String arg5) {
			}
		});
		webClient.getOptions().setThrowExceptionOnScriptError(false);
	}

	public WebClient getWebClient() {
		return webClient;
	}

}

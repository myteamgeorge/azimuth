package com.crawler.sistemasinternos.crawlers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.crawler.sistemasinternos.dto.QualOperadoraResponse;
import com.crawler.sistemasinternos.enumerator.URL;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.html.HtmlButton;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTelInput;

@Component
public class QualOperadoraCrawler extends BaseCrawler {

	public QualOperadoraCrawler() throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		super(URL.QUAL_OPERADORA.getPath(), false);
	}

	public QualOperadoraCrawler(String url) throws FailingHttpStatusCodeException, MalformedURLException, IOException {
		super(url);
	}

	public QualOperadoraResponse pesquisar(String numero) throws IOException, InterruptedException {

		QualOperadoraResponse retorno = new QualOperadoraResponse();

		checkCaptcha(this.page);

		HtmlTelInput inputTelefone = (HtmlTelInput) this.page.getFirstByXPath("//*[@id='tel']");
		HtmlButton submitButton = (HtmlButton) this.page.getFirstByXPath("//*[@id='bto']");

		inputTelefone.setText(numero);

		HtmlPage pesquisa = (HtmlPage) submitButton.click();

		checkCaptcha(pesquisa);

		HtmlDivision divResultado = null;
		int count = 0;
		while (count++ < 10 && divResultado == null) {

			this.getWebClient().waitForBackgroundJavaScript(1000);
			divResultado = (HtmlDivision) pesquisa.getFirstByXPath("//*[@id='ctd']");
		}

		List<String> dados = new ArrayList<>();

		List<HtmlElement> imgs = divResultado.getElementsByTagName("img");
		if (!imgs.isEmpty()) {
			HtmlImage image = (HtmlImage) imgs.get(0);

			String operadora = image.getSrcAttribute().toString();
			dados.add(getNomeOperadora(operadora));
		}

		List<HtmlElement> infosTela = divResultado.getElementsByTagName("p");
		for (HtmlElement infos : infosTela) {

			String info = infos.asText();
			dados.add(info.split(":")[1]);
		}

		retorno.setNumero(numero);
		retorno.setOperadora(dados.get(0));
		retorno.setPortabilidade(dados.get(1));
		retorno.setEstado(dados.get(2));
		retorno.setRegiao(dados.get(3));

		return retorno;
	}

	private void checkCaptcha(HtmlPage page) throws IOException {

		HtmlDivision captcha = (HtmlDivision) page.getFirstByXPath("//*[@class='recaptcha-checkbox-checkmark']");

		if (captcha != null) {
			captcha.click();
			System.out.println("999999");
		}

	}

	private String getNomeOperadora(String operadora) {
		String filtrado = "";

		String[] dados = operadora.split("/");

		filtrado = dados[dados.length - 1].split("\\.")[0];

		return filtrado;
	}

	public static void main(String[] args)
			throws FailingHttpStatusCodeException, MalformedURLException, IOException, InterruptedException {

		QualOperadoraCrawler crawler = new QualOperadoraCrawler();
		crawler.pesquisar("41999752255");

	}

}

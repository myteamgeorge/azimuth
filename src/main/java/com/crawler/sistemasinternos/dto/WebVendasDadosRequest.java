package com.crawler.sistemasinternos.dto;

import java.io.Serializable;
import java.util.Date;

public class WebVendasDadosRequest implements Serializable {

	private static final long serialVersionUID = 2777447689355030924L;

	public WebVendasDadosRequest() {
	}

	private String documento;

	private Date date;

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}

package com.crawler.sistemasinternos.dto;

import java.io.Serializable;

public class QualOperadoraResponse implements Serializable {

	private static final long serialVersionUID = -8479225502343751932L;

	public QualOperadoraResponse() {
	}

	private String numero;

	private String operadora;

	private String portabilidade;

	private String estado;

	private String regiao;

	private String error;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getOperadora() {
		return operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

	public String getPortabilidade() {
		return portabilidade;
	}

	public void setPortabilidade(String portabilidade) {
		this.portabilidade = portabilidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}

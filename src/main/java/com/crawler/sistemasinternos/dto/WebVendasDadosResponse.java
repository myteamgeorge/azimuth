package com.crawler.sistemasinternos.dto;

import java.io.Serializable;

import com.crawler.sistemasinternos.model.WebVendasDados;

public class WebVendasDadosResponse implements Serializable {

	private static final long serialVersionUID = 2777447689355030924L;

	public WebVendasDadosResponse() {
	}

	private String documento;

	private int status;

	private String error;

	private WebVendasDados dados;

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public WebVendasDados getDados() {
		return dados;
	}

	public void setDados(WebVendasDados dados) {
		this.dados = dados;
	}

}

package com.crawler.sistemasinternos.helper;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateHelper {
	
	public static String getFormatedDate() {
		LocalDateTime ld = LocalDateTime.now(ZoneId.of( "America/Sao_Paulo" ));
	
		return ld.format(DateTimeFormatter.ofPattern("dd/MM/y HH:mm:ss"));
	}

}

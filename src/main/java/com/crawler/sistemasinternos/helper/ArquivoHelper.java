package com.crawler.sistemasinternos.helper;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

import com.crawler.sistemasinternos.model.Acesso;

public class ArquivoHelper {
	
	private final static String fileName = "acesso.txt";

	public static BufferedReader carregarArquivo() throws FileNotFoundException {
		FileReader arq = new FileReader(fileName);
		BufferedReader lerArq = new BufferedReader(arq);
		return lerArq;
	}

	public static String carregarLoginSenhaPrimeiraLinha() throws Exception {
        BufferedReader lerArq = carregarArquivo();
    
        String linha = lerArq.readLine();
        
        lerArq.close();
        
        return linha;
	}
	
	public static int carregarQuantidadeRobos() throws Exception {
        BufferedReader lerArq = carregarArquivo();
        
        lerArq.readLine();
        int linha = Integer.valueOf(lerArq.readLine());
        
        lerArq.close();
        
        return linha;
	}

	public static boolean salvarAcesso(Acesso acesso) throws Exception {
		FileWriter arq = new FileWriter(fileName);
		
		StringBuilder novo = new StringBuilder();
		novo.append(acesso.getLogin().replaceAll(" ",  "")).append(";").append(acesso.getSenha().replaceAll(" ",  ""));
		novo.append("\n").append(acesso.getRobos());
		
        arq.write(novo.toString());
        arq.close();
    
        return true;
	}

}

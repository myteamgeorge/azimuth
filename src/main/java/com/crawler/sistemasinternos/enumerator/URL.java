package com.crawler.sistemasinternos.enumerator;

public enum URL {

	WEBVENDAS("https://webvendas.gvt.com.br"),
	WEBVENDAS_LIST("webVendas/list"),
	WEBVENDAS_CARREGAR("webVendas/get"),
	WEBVENDAS_DOWNLOAD("webVendas/download"),
	
	QUAL_OPERADORA("http://qualoperadora.info/");
	
	private URL(String path) {
		this.path = path;
	}

	private String path;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}

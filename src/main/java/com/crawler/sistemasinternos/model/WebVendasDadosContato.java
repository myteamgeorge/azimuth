package com.crawler.sistemasinternos.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 * @author George Neto
 */
@Entity
public class WebVendasDadosContato implements Serializable {

	private static final long serialVersionUID = -2117913557135042903L;

	public WebVendasDadosContato() {
	}
	
	public String[] asArray() {
		String[] retorno = {
				this.contato != null ? this.contato : "",
				this.telefone1 != null ? this.telefone1 : "",
				this.telefone2 != null ? this.telefone2 : "",
				this.telefone3 != null ? this.telefone3 : "",
				this.email != null ? this.email : "" };
		
		return retorno;
	}

	public WebVendasDadosContato(String contato) { 
		
		this.contato = contato;
		
		/*if (contato != null) {
			
			if (contato.contains("@")) {
				this.email = contato;
			} else {
				preencheTelefone(contato);
			}
			
			if (contato.contains("-")) {
				String contatoDados[] = contato.split("-");

				for (int a = 1; a < contatoDados.length; a++) {

					String valor = contatoDados[a];

					if (valor.contains("@")) {
						this.email = valor;
					} else {
						preencheTelefone(valor);
					}

				}

			} else {
				this.contato = contato;
			}
		}*/

	}

	private void preencheTelefone(String telefone) {
		if (this.telefone1 == null) {
			this.telefone1 = telefone;
		} else if (this.telefone2 == null) {
			this.telefone2 = telefone;
		} else if (this.telefone3 == null) {
			this.telefone3 = telefone;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Size(min = 5, max = 100)
	private String contato;

	@Size(min = 5, max = 100)
	private String telefone1;

	@Size(min = 5, max = 100)
	private String telefone2;

	@Size(min = 5, max = 100)
	private String telefone3;

	@Size(min = 5, max = 100)
	private String email;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public String getTelefone3() {
		return telefone3;
	}

	public void setTelefone3(String telefone3) {
		this.telefone3 = telefone3;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}

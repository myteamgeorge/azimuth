package com.crawler.sistemasinternos.model;

import java.io.Serializable;

public class Acesso implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String login;
	private String senha;
	private Integer robos;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Integer getRobos() {
		return robos;
	}

	public void setRobos(Integer robos) {
		this.robos = robos;
	}

    public void setLoginSenhaFromString(String loginSenhaString) {
		String[] dados = loginSenhaString.split(";");
		this.login = dados[0];
		this.senha = dados[1];
    }
}

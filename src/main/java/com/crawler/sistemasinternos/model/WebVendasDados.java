package com.crawler.sistemasinternos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author George Neto
 */
@Entity
public class WebVendasDados implements Serializable {

	private static final long serialVersionUID = 7526069117884756634L;

	public WebVendasDados() {
		this.webVendasDadosInstalacao = new ArrayList<>();
		this.webVendasDadosContato = new ArrayList<>();
	}

	public Boolean isAtivo() {
		for (WebVendasDadosInstalacao dadosInstalacao : this.webVendasDadosInstalacao) {
			if (dadosInstalacao.isAtivo()) {
				return Boolean.TRUE;
			}
		}

		return Boolean.FALSE;
	}

	public WebVendasDadosInstalacao getAtivo() {
		for (WebVendasDadosInstalacao dadosInstalacao : this.webVendasDadosInstalacao) {
			if (dadosInstalacao.isAtivo()) {
				return dadosInstalacao;
			}
		}

		return null;
	}

	public WebVendasDadosInstalacao getNaoAtivo() {
		
		List<WebVendasDadosInstalacao> naoAtivos = new ArrayList<>();
		WebVendasDadosInstalacao retorno = new WebVendasDadosInstalacao();
		
		for (WebVendasDadosInstalacao dadosInstalacao : this.webVendasDadosInstalacao) {
			if (!dadosInstalacao.isAtivo()) {
				naoAtivos.add(dadosInstalacao);
			}
		}

		if(!naoAtivos.isEmpty()){
			retorno = naoAtivos.get(0);
		}
		
		return retorno;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	private Date data;

	@Size(min = 5, max = 100)
	private String documento;

	@Size(min = 5, max = 100)
	private String nome;

	@Size(min = 5, max = 100)
	private String segmento;

	@Size(min = 5, max = 100)
	private String horaContatoInicial;

	@Size(min = 5, max = 100)
	private String horaContatoFinal;

	@OneToMany
	private List<WebVendasDadosContato> webVendasDadosContato;

	@OneToMany
	private List<WebVendasDadosInstalacao> webVendasDadosInstalacao;


	public Object[] asArray() {
		List<String> dados = new ArrayList<>();
 		dados.add(documento);
 		dados.add(nome);
 		dados.add(segmento);
 		dados.add(horaContatoInicial);
 		dados.add(horaContatoFinal);
 		
 		WebVendasDadosInstalacao dadosIntalacao = this.isAtivo() ? this.getAtivo() : this.getNaoAtivo(); 
 		if(dadosIntalacao != null){
	 		dados.add(dadosIntalacao.getEndereco());
	 		dados.add(dadosIntalacao.getRede());
	 		dados.add(dadosIntalacao.getLinha());
	 		dados.add(dadosIntalacao.getPlano());
	 		dados.add(dadosIntalacao.getBandaLarga());
	 		dados.add(dadosIntalacao.getTv());
	 		dados.add(dadosIntalacao.getLinhaMovel());
	 		dados.add(dadosIntalacao.getStatus());
	 		dados.add(dadosIntalacao.getDataConclusao());
		}
 		
 		int count = 0;
 		for(WebVendasDadosContato contato : this.webVendasDadosContato){
 			count++;
 			
 			dados.add(contato.getContato());
 			
 			if(count > 4){
 				break;
 			}
 		}
 		for(WebVendasDadosContato contato : this.webVendasDadosContato){
 			for(String dadoContato : contato.asArray()){
 	 			dados.add(dadoContato);
 			}
 		}
		return dados.toArray();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSegmento() {
		return segmento;
	}

	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}

	public String getHoraContatoInicial() {
		return horaContatoInicial;
	}

	public void setHoraContatoInicial(String horaContatoInicial) {
		this.horaContatoInicial = horaContatoInicial;
	}

	public String getHoraContatoFinal() {
		return horaContatoFinal;
	}

	public void setHoraContatoFinal(String horaContatoFinal) {
		this.horaContatoFinal = horaContatoFinal;
	}

	public List<WebVendasDadosContato> getWebVendasDadosContato() {
		return webVendasDadosContato;
	}

	public void setWebVendasDadosContato(List<WebVendasDadosContato> webVendasDadosContato) {
		this.webVendasDadosContato = webVendasDadosContato;
	}

	public List<WebVendasDadosInstalacao> getWebVendasDadosInstalacao() {
		return webVendasDadosInstalacao;
	}

	public void setWebVendasDadosInstalacao(List<WebVendasDadosInstalacao> webVendasDadosInstalacao) {
		this.webVendasDadosInstalacao = webVendasDadosInstalacao;
	}

}

package com.crawler.sistemasinternos.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 * @author George Neto
 */
@Entity
public class WebVendasDadosInstalacao implements Serializable {

	private static final long serialVersionUID = 2804343289495364974L;

	public WebVendasDadosInstalacao() {
	}

	public WebVendasDadosInstalacao(List<String> values) {

		int coluna = 0;

		this.selecionado = values.get(coluna++).contains("selected");
		this.endereco = values.get(coluna++);
		this.rede = values.get(coluna++);
		this.tecnologia = values.get(coluna++);
		this.linha = values.get(coluna++);
		this.plano = values.get(coluna++);
		this.bandaLarga = values.get(coluna++);
		this.tv = values.get(coluna++);
		this.linhaMovel = values.get(coluna++);
		this.status = values.get(coluna++);

	}

	public String[] asArray() {
		String[] retorno = { selecionado.toString(), endereco, rede, tecnologia, linha, plano, bandaLarga, tv,
				linhaMovel, status, dataConclusao };

		return retorno;
	}

	public Boolean isAtivo() {
		return (this.status != null && (this.status.replaceAll(" ", "").equalsIgnoreCase("ativo")
				|| this.status.replaceAll(" ", "").equalsIgnoreCase("enviado")));
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private Boolean selecionado;

	@Size(min = 5, max = 100)
	private String endereco;

	@Size(min = 5, max = 100)
	private String rede;

	@Size(min = 5, max = 100)
	private String tecnologia;

	@Size(min = 5, max = 100)
	private String linha;

	@Size(min = 5, max = 100)
	private String plano;

	@Size(min = 5, max = 100)
	private String bandaLarga;

	@Size(min = 5, max = 100)
	private String tv;

	@Size(min = 5, max = 100)
	private String linhaMovel;

	@Size(min = 5, max = 100)
	private String status;

	@Size(max = 100)
	private String dataConclusao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Boolean getSelecionado() {
		return selecionado;
	}

	public void setSelecionado(Boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getRede() {
		return rede;
	}

	public void setRede(String rede) {
		this.rede = rede;
	}

	public String getTecnologia() {
		return tecnologia;
	}

	public void setTecnologia(String tecnologia) {
		this.tecnologia = tecnologia;
	}

	public String getLinha() {
		return linha;
	}

	public void setLinha(String linha) {
		this.linha = linha;
	}

	public String getPlano() {
		return plano;
	}

	public void setPlano(String plano) {
		this.plano = plano;
	}

	public String getBandaLarga() {
		return bandaLarga;
	}

	public void setBandaLarga(String bandaLarga) {
		this.bandaLarga = bandaLarga;
	}

	public String getTv() {
		return tv;
	}

	public void setTv(String tv) {
		this.tv = tv;
	}

	public String getLinhaMovel() {
		return linhaMovel;
	}

	public void setLinhaMovel(String linhaMovel) {
		this.linhaMovel = linhaMovel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(String dataConclusao) {
		this.dataConclusao = dataConclusao;
	}

}

angular.module('AzimuthApp').controller('HomeController', ['$scope', 'AzimuthService', '$window', function($scope, AzimuthService, $window){

	var c = this;
	
	c.myFile = '';
	c.documentos = '';

	c.isAlterarSenha = false;
	
	c.carregar = function(myFile){
		
		var mensagemEnviando = "Enviando arquivo para o servidor...";
		
		c.mensagemCarregando = mensagemEnviando;
		c.isLoading = true;
		
		AzimuthService.uploadFileToUrl(myFile, c.callbackUpload, c.callbackErrorUpload);
		
		console.log(mensagemEnviando + myFile);
	}
	
	c.callbackUpload = function(response){
		
		console.log("Sucesso no upload!");
		c.mensagemCarregando = "Arquivo enviado! Os documentos estão sendo processados...";
		
		AzimuthService.processar(c.callbackProcessar, c.callbackErrorUpload);
		
	}
	
	c.callbackProcessar = function(data, $q){
	    
		c.mensagemCarregando = "";
		c.isLoading = false;
	}
	
	c.callbackErrorUpload = function(response){
		
		var mensagem = "ERRO!" + JSON.stringify(response);
		console.log(mensagem);
		alert(mensagem);
		c.isLoading = false;
	}
	
	c.download = function(){

		c.mensagemCarregando = "Iniciando download de último arquivo processado...";
		c.isLoading = true;
		
		AzimuthService.download(undefined, c.callbackProcessar);
	}
	
	c.alterarSenha = function(){
		c.isAlterarSenha = true;
	}
	
	c.salvarSenha = function(){
		c.isLoading = false;
		AzimuthService.salvarSenhaNova(
			c.acesso, 
			function(data){ 
				alert('Nova senha salva com sucesso'); 
				c.isLoading = false;
				c.isAlterarSenha = false;
			},
			function(data){ 
				alert('Erro ao salvar senha'); 
				c.isLoading = false;
			}
		);
	}

	c.salvarQuantidadeRobos = function(){

	    $scope.mensagemRobo = '';

        const limit = 30;

	    if(c.acesso.robos && c.acesso.robos > limit){
	         $scope.mensagemRobo = `O limite é ${limit} robôs rodando...`
	         return;
	    }

		c.isLoading = false;
		AzimuthService.salvarQuantidadeRobos(
			c.acesso,
			function(data){
				alert('Nova quantidade de robos salva com sucesso. Essa quantidade só será válida em novos processamentos (se já existir algum rodando, não vai ser alterado)');
				c.isLoading = false;
				c.isAlterarSenha = false;
			},
			function(data){
				alert('Erro ao salvar nova quantidade de robos');
				c.isLoading = false;
			}
		);
	}

	c.carregarDadosAcesso = function(){

        c.acesso = {}

	    AzimuthService.carregarDadosAcesso(
            function(data){
                c.acesso = data.data
            },
            function(data){
                console.log('Falha ao carregar DadosAcesso atual');
            }
        );

	}

	c.voltar = function() {
        c.isAlterarSenha = false;
        c.isLoading = false;
    }


    //chamando carregamento inicial de dados
	c.carregarDadosAcesso();
	
} ] );
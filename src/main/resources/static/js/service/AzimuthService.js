angular.module('AzimuthApp').service('AzimuthService', ['$http', '$q', function($http, $q){

	var _getUrl = function(){
		var retorno = '';

		var urlAtual = window.location.href;
		var portAtual = window.location.port;
		
		return urlAtual;
	}
	
	var _processar = function(callback, callbackError){
		
		var urlRequest = _getUrl() + 'webVendas/processar/';
		
		$http({
				url: urlRequest,
			    method: 'GET',
			    responseType: 'arraybuffer',
			    headers: {
			        'Content-type': 'application/json',
			        'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			    }
			})
			.then(
					function(response){
						_download(response, callback) 
					} , 
        			callbackError);
		return defered.promise;
	}
	
	var _list = function(documentos, callback, callbackError){
		
		var urlRequest = _getUrl() + 'webVendas/list/';
		
		var processados = [];
		
		var docs = documentos.split(',');

		for(var a =0; a<docs.length; a++){
			var obj = {'documento': docs[a]};
			processados.push( obj );
		}

		$http.post(urlRequest, processados)
			.then(callback, callbackError);
		
	}
	

	var _uploadFileToUrl = function(file, callback, callbackError){

        var uploadUrl = _getUrl() + 'upload/';
        
        var fd = new FormData();
        fd.append('file', file);
        var opt = {
	                transformRequest: angular.identity,
	                headers: {'Content-Type': undefined}
        		};
        
        $http.post(uploadUrl, fd, opt)
        	.then(callback, callbackError);
    }
	
	var _download = function(response, callback){
		var defered = $q.defer();
		
		if(response){
			defered.resolve(response);
		}
			
		var fileName = 'download.xlsx';
		
	    var a = document.createElement("a");
	    document.body.appendChild(a); 
	    
	    var url = _getUrl() + 'webVendas/download/';
	    
        a.href = url;
        a.download = fileName;
        a.click();
        window.URL.revokeObjectURL(url);

        if(callback){
        	callback();
        }
	        
	}
	
	var _salvarSenhaNova = function(acesso, callback, callbackError){
		
		var urlRequest = _getUrl() + 'novo-acesso';

		$http.post(urlRequest, {login: acesso.login, senha: acesso.senha })
			.then(callback, callbackError);
		
	}

	var _salvarQuantidadeRobos = function(acesso, callback, callbackError){

		var urlRequest = _getUrl() + 'quantidade-robos';

		$http.post(urlRequest, {robos: acesso.robos })
			.then(callback, callbackError);

	}

	var _carregarDadosAcesso = function(callback, callbackError){

		var urlRequest = _getUrl() + 'acesso';

		$http.get(urlRequest).then(callback, callbackError);

	}
	
	return {
		'list': _list,
		'uploadFileToUrl': _uploadFileToUrl,
		'processar': _processar,
		'download': _download,
        'salvarSenhaNova': _salvarSenhaNova,
        'salvarQuantidadeRobos': _salvarQuantidadeRobos,
        'carregarDadosAcesso': _carregarDadosAcesso
	}
	
} ]);